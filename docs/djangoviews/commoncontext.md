동일한 내용의 context 데이터가 필요한 view가 많다고 가정해 보자. 어떻게 처리해야 할까?

몇 가지 다른 해답이 있다.

1. 여러분의 사이트 거의 모든 페이지에서 데이터가 필요할까? 정답은 [context processors](https://docs.djangoproject.com/en/stable/ref/templates/api/#django.template.RequestContext)이다.
2. 데이터가 사이트 대부분에서 필요하지만 모든 곳에서 필요한 것은 아니며 이를 결정하는 데 수고가 많이 들까? [lazy evaluation in your context processor](https://stackoverflow.com/a/28146359/182604)를 사용하는 것을 추천한다.
3. template 수준에 실제로 존재하는 "component"에 대한 데이터가 필요할까? 아니면 기본 template에 포함되어 있는가? 예를 들어, 많은 페이지의 머리글 또는 바닥글에 나타나는 공통 요소에 필요한 데이터일 수 있다.

    일반적으로 이 작업은 자체 데이터를 로드할 수 있는 [custom inclusion template tag](https://docs.djangoproject.com/en/stable/howto/custom-template-tags/#inclusion-tags/)를 사용하여 가장 쉽게 수행할 수 있으므로 이 구성 요소를 포함할 때마다 view 함수를 변경할 필요가 없다.

그러나 이 중 어느 것도 해당되지 않는다고 가정한다. 페이지 그룹에 사용되는 몇 가지 공통 데이터가 있을 뿐이다. 아마도 야러분은 전자상거래 사이트를 가지고 있고, 모든 체크아웃 페이지는 그들이 필요로 하는 공통된 데이터 세트를 가지고 있을 것이다. 반드시 같은 방식으로 표시하지 않아도 된다.

이를 위해, 공통 데이터를 함수로 반환하는 코드를 추출하는 아래의 간단한 방법을 사용할 수 있다.

```
def checkout_start(request):
    context = {
        # etc
    } | checkout_pages_context_data(request.user)
    return TemplateResponse(request, "shop/checkout/start.html", context)


def checkout_pages_context_data(user):
    context = {}
    if not user.is_anonymous:
        context["user_addresses"] = list(user.addresses.order_by("primary", "first_line"))
    return context
```

필요한 모든 view에 `| checkout_pages_context_data(request.user)`를 추가하기만 하면 된다.

이는 매우 사용하기 쉽고, 이해하기 쉽고, 완벽하게 유연한 적절한 방법이다. 위와 같이 `user` 객체에 따라 필요한 경우 함수에 매개 변수를 추가하고 요구 사항에 따라 이러한 helper들의 공통 집합을 더 큰 helper로 결합할 수 있다. 그리고 이 helper들에게 중요한 논리가 있다면 테스트를 작성할 수도 있다.

다음: [view내 URL 파라미터](urlparameters.md)

## <a name='discussion'>토론: Helpers vs mixins</a>
Not yet, Comming Soon!
