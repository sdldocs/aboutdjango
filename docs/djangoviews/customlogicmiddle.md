만약 여러분이 코드 "중간에" 다른 것을 하고 싶다면 어떻게 하면 될까?

여러분은 지금 더 발전된 영역으로 들어가고 있기 때문에, 이 페이지는 이전에 왔던 것들보다 더 무겁지만, 여기에 있는 기술들을 또한 매우 강력하고 광범위하게 적용할 수 있다.

[product 목록을 포함하는 두 가지 다른 view의 예](https://spookylukey.github.io/django-views-the-right-way/delegation.html)에서 실제 프로젝트에서 발생할 수 있는 복잡성을 모방하여 새로운 요구 사항을 추가해 보겠다.

Django의 `QuerySets`를 product 목록의 기초로 사용하는 대신, 여러분은 다른 API를 사용해야 한다. 아마도 그것은 서드파티 HTTP 기반의 서비스이거나, 여러분의 자체 서비스일 수 있지만, 여러분은 `QuerySet`을 입력으로 받아들이지 않는 기능에서 시작한다. 아마도 다음과 같을 것이다.

```
def product_search(filters, page=1):
    ...
    return product_list
```

`filter`는 product 필터링 정보를 포함하는 사전이며, 허용 가능한 키는 다른 곳에 정의되어 있다. 이제 `display_product_list`는 `request.GET` 쿼리 문자열 매개 변수를 `filters`로 전달할 수 있는 항목으로 변환해야 한다.

(간단하게 하기 위해 `Paginator`가 페이지 수 등과 함께 제공하는 것과 달리 이 예에서는 훨씬 더 기본적인 종류의 페이징을 수행한다.)

그러나 special offers의 경우 추가 매개 변수가 필요한 **다른** 함수를 제공하였다.

```
def special_product_search(filters, special_offer, page=1):
    ...
    return product_list
```

또한 special offers 페이지의 경우 표시될 product 목록을 검색한 다음, 데이터베이스 로깅을 수행하여 사용자, special offers 및 디스플레이된 product를 기록해야 한다.

이 모든 것의 요점은 view 기능뿐만 아니라 많은 프로그래밍 상황에 적용되는 공통 요구사항을 설정하는 것이다.

**여러분은 공통된 논리의 중간에서 사용자 정의 논리를 어떻게 실행할 수 있을까요?**

이것이 단지 [parameterization](https://www.toptal.com/python/python-parameterized-design-patterns)의 또 다른 예라고 생각할 수 있다. 여러분에게는 "중간에 해야 할 일"을 포착할 수 있는 매개 변수가 필요하다.

더 쉬운 경우부터 시작하자. [이전](https://spookylukey.github.io/django-views-the-right-way/delegation.html)과 마찬가지로 메인 view에 요소화된 `product_list` view와 그것이 위임하는 `display_product_list` 함수이다. 후자는 이제 변화가 필요하다.

1. `queryset` 매개 변수를 더 이상 사용하지 않고, `searcher` 매개 변수를 사용한다.
2. `QuerySet`에서 전달된 데이터를 조작하는 대신 이 `searcher` 매개 변수를 사용하도록 조정해야 한다.

다음과 같다.

```
from somewhere import product_search

def product_list(request):
    return display_product_list(
        request,
        searcher=product_search,
        template_name='shop/product_list.html',
    )

def display_product_list(request, *, context=None, searcher, template_name):
    if context is None:
        context = {}
    filters = collect_filtering_parameters(request)
    try:
        page = int(request.GET['page'])
    except (KeyError, ValueError):
        page = 1
    context['products'] = searcher(filters, page=page)
    return TemplateResponse(request, template_name, context)
```

조금 설명하자면, 여기서는 `product_search` 함수를 `display_product_list`에 `searcher` 매개 변수로 전달했다. 이 기능을 “first class functions”이라고 한다. 매개 변수로 다른 데이터를 전달할 수 있는 것처럼 함수도 전달할 수 있다. 그것이 이 방법에서 기술의 핵심이며, 여러분이 공통 논리의 중간에 여러분의 사용자 정의 논리를 삽입할 수 있게 한다.

하지만 `special_offer_detail`에서는 어떨까? `searcher=special_product_search`를 통과하면 `display_product_list` 내부에서 문제가 발생한다. 전달된 함수는 다음과 같이 호출된다.

```
searcher(filters, page=page)
```

그러나 이는 추가 매개 변수가 있는 `special_product_search`의 시그니처와 일치하지 않는다. 그 매개변수를 어떻게 통과시킬 수 있을까?

`display_product_list`가 필요한 추가 매개 변수를 수락하도록 유도할 수도 있지만, 이것은 바람직하지 않다. 우리는 사용하지 않는 매개 변수를 다른 곳으로 전달할 수 있도록 전달해야 한다. 게다가 그것은 불필요하다.

대신, 우리가 해야 하는 것은 `special_off_detail`이 `display_product_list`가 `searcher`에게 기대하는 시그니처와 일치하는 어댑터 기능을 제공하는 것이다. 어댑터 함수 내에서 `special_product_search` 함수가 필요한 방식으로 호출할 것이다. 이 작업을 수행하는 동안 추가 요구사항도 수행할 수 있다.

추가 로깅을 위해 `log_special_offer_product_view` 함수를 작성했다고 가정하면 다음과 같다.

```
from somewhere import special_product_search

def special_offer_detail(request, slug):
    special_offer = get_object_or_404(SpecialOffer.objects.all(), slug=slug)

    def special_product_search_adaptor(filters, page=1):
        products = special_product_search(filters, special_offer, page=page)
        log_special_offer_product_view(request.user, special_offer, products)
        return products

    return display_product_list(
        request,
        context={
            'special_offer': special_offer,
        },
        searcher=special_product_search_adaptor,
        template_name='products/special_offer_detail.html',
    })
```

이에 대해 몇 가지 중요한 사항이 있다.

- 메인 view 본체 내부에 어댑터 함수 `special_product_search_adapter`를 정의했다. 이는 다음 기능을 위해 중요하다. (다른 방법도 있지만 이것이 가장 간단하다.)
- 우리는 그것의 시그니처를 `display_product_list`가 예상한 것과 일치하도록 만들었다.
- 우리의 어댑터 함수는 스코프 내부로 부터 `special_offer` 객체와 `request`를 액세스할 수 있다. 어댑터 함수가 `display_product_list`로 전달될 때 이러한 객체들은 "stay with it"하므로 일반 인수로 전달되지 않았음에도 불구하고 사용할 수 있다.

    이러한 방식으로 동작하는 함수를 "closure"라고 하며, 스코프 내부에서 변수를 캡처한다.

이 강력한 기술은 많은 장점을 가지고 있다. 우선, `display_product_list`는 이 모든 것에 관여할 필요가 없다. 우리는 그것의 시그니처나 그것이 기대하는 `searcher` 매개변수의 시그니처를 수정할 필요가 없다. 또한 이 기능은 정의되지 않은 이름 등을 지적할 수 있는 많은 IDE에 기본으로 제공되는 linter와 같은 정적 분석에서도 매우 잘 작동한다.

클로져(closure)는 일부 사람들이 어렵다고 생각하는 개념이지만, 다양한 프로그래밍 상황에서 매우 유용하다. 위의 내용이 혼란스러웠다면 이 [Python closures primer](https://www.programiz.com/python-programming/closure)를 읽은 다음 더 복잡한 여기 예로 돌아오세요.

논리를 재사용한다는 주제에서 [Preconditions](preconditions.md)을 다루려고한다. 그 전에 몇 가지 기본적인 것을 돌아 본다. 첫 번째는 [Redirects](redirects.md)이고 다음은 [Forms](forms.md)이다.

## <a name='terminology'>Notes: 용어(terminology)</a>
객체 지향 언어에서 이 문제에 대한 표준 솔루션은 “strategy pattern”이다. 여기에는 수행해야 하는 작업을 캡슐화할 수 있는 객체를 만드는 작업이 포함된다.

Python에서 함수는 “first class objects“, 즉 다른 모든 유형의 값과 마찬가지로 전달할 수 있는 객체이다. 그래서 우리는 “strategy pattern”이 필요할 때 "함수"를 사용할 수 있다. 특히 우리의 전략이 한 부분만 포함된 경우에는 더욱 그렇다. 함께 번들해야 하는 둘 이상의 엔트리 포인트가 있는 경우 클래스가 유용할 수 있다.

좀 더 일반적인 개념은 "dependency injection"이다. 만약 당신이 무언가를 해야 하는 어떤 코드를 가지고 있다면, 즉 그것에 직접적으로 의존하는 대신에 다른 코드에 의존한다, 즉 그 의존성은 외부로부터 주입된다. 만약 우리의 의존성이 단지 하나의 함수 호출이라면, 우리는 단순히 매개 변수로 함수를 사용할 수 있다. 종속성에 관련 함수의 호출이 집합인 경우 매개 변수로서 메서드를 갖는 객체를 원할 수 있다.

("dependency injection"이라는 용어가 한 단계 더 나아가 어떤 식으로든 의존성 **자동** 주입에 사용되는 경우가 종종 있다. 저자는 이것들을 "dependency injection frameworks/containers"라고 부른다. [pytest’s fixtures](https://docs.pytest.org/en/latest/fixture.html)를 제외하고는 저자는 아직 Python에서 이것들에 대한 필요성이나 욕구를 찾지 못했다.)

그래서 우리는 이 패턴을 "first class functions" 또는 "callbacks”", "strategy pattern" 또는 "dependency injection"이라고 부를 수 있다. 하지만 dependency injection이 가장 쿨하게 보이기 때문에 저자는 그것을 제목에 사용했다.

## <a name='di-vs-inheritance'>토론: DI vs 상속</a>
Not yet, Comming Soon!
