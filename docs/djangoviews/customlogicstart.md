다음 몇 페이지에서는 한 view에서 다른 view의 일부 로직을 재사용해야 하는 문제를 설명한다. 유틸리티 기능과 클래스를 어떻게 사용할 수 있는 지에 대해 생각해 보았지만, 때로는 이러한 기능이 유용하지 않아서 view 본문 대부분을 재사용해야 할 수도 있다. FBV로 어떻게 할 수 있을까?

product 목록의 예제를 계속 진행하면서 변형을 추가해 보겠다. 주요 product 목록 페이지뿐만 아니라 "special offers" 페이지도 제공하려고 한다. 다양한 product를 제공할 수 있는 special offers 모델이 있기 때문이다. 각 페이지에는 special offers에 대한 세부 정보와 해당 행사와 관련된 product 목록이 표시되어야 한다. 기능 요구 사항은 이 product 목록이 product 일상 목록의 모든 기능(필터링, 정렬 등)을 가지고 있어야 한다고 하므로, 가능한 한 로직을 재사용하고 싶다.

따라서 view는 두 가지 작업을 수행해야 한다. 하나의 객체를 보여주는 것이고, 다른 하나는 목록을 보여주는 것이다. FBV로 두 가지 일을 하는 방법에 대한 답은 두 가지 일을 모두 하는 것이다. 그것을 위해 특별한 테크닉이 필요하지 않고, 다음과 같은 간단한 버전의 view로 시작하겠다.

```
# urls.py

from . import views

urlpatterns = [
    path('special-offers/<slug:slug>/', views.special_offer_detail, name='special_offer_detail'),
]
```

```
# views.py

def special_offer_detail(request, slug):
    special_offer = get_object_or_404(SpecialOffer.objects.all(), slug=slug)
    return TemplateResponse(request, 'shop/special_offer_detail.html', {
        'special_offer': special_offer,
        'products': special_offer.get_products(),
    })
```

`SpecialOffer.get_products()` 메서드가 존재한다고 가정하고 `QuerySet`을 반환한다. 적절한 `ManyToMany` 관계가 있는 경우 구현이 `returnself.products.all()`만큼 간단할 수 있지만 다를 수도 있다.

그러나 이제 우리는 이 view가 필터링/정렬/페이징이든 또는 지금까지 작성된 다른 모든 것이든 일반 `product_list` view의 로직을 재사용하도록 변경하고자 한다(아래 함수 `apply_product_filtering()`). 어떻게 하면 좋을까?

한 가지 방법은 [공통 context 데이터](commoncontext.md)에서 수행한 작업을 수행하는 것이다. 기존 `product_list` view의 일부를 매개 변수들을 사용하여 context에 추가할 데이터를 반환하는 함수로 이동한다. 하지만, 때때로 그 인터페이스가 작동하지 않을 때가 있다. 예를 들어, view가 어떤 경우에 완전히 다른 종류의 응답(예: redirection)을 반환한다고 결정하면 공통 논리가 해당 틀에 맞지 않는다.

대신 저자는 **delegation**이라고 부르는 것을 사용하겠다. 진입점 view를 통해 나머지 작업을 다른 기능에 위임할 수 있다.

이 기능을 만들려면 이전 `product_list` view를 보고 [parameterization](https://www.toptal.com/python/python-parameterized-design-patterns)을 적용하여야 한다. 전달해야 할 추가 매개 변수는 product 목록 `QuerySet`, 사용할 template 이름 및 추가 context 데이터이다. 이러한 기능을 사용하면 `display_product_list` 함수를 쉽게 이용할 수 있으며, 두 엔트리 포인트 view 함수에서 이를 호출할 수 있다.

```
def product_list(request):
    return display_product_list(
        request,
        queryset=Product.objects.all(),
        template_name='shop/product_list.html',
    )


def special_offer_detail(request, slug):
    special_offer = get_object_or_404(SpecialOffer.objects.all(), slug=slug)
    return display_product_list(
        request,
        context={
            'special_offer': special_offer,
        },
        queryset=special_offer.get_products(),
        template_name='shop/special_offer_detail.html',
    )


def display_product_list(request, *, context=None, queryset, template_name):
    if context is None:
        context = {}
    queryset = apply_product_filtering(request, queryset)
    context |= paged_object_list_context(request, queryset, paginate_by=5)
    return TemplateResponse(request, template_name, context)
```

> **Note**: <br>
> `display_product_list`의 시그니처에 익숙하지 않은 사용자를 위해
>
> - `*` 뒤의 인수는 키워드 전용 인수이다.
> - `queryset`과 `template_name`에 (적절한 기본값이 없어) 인수 값을 호출할 때 제공해야 한다.
> - `context`에 대해 우리는 합리적인 기본값을 가지고 있지만 [변경 가능한 기본 인수 gotcha](https://docs.python-guide.org/writing/gotchas/#mutable-default-arguments)를 피해야 하기 때문에 시그니처에서 없음을 사용하고 나중에 {}(으)로 변경한다.

template 수준에서는 중복 요소를 제거하기 위해 [include](https://docs.djangoproject.com/en/stable/ref/templates/builtins/#include)를 사용하여 유사한 리팩토링을 수행할 수 있다.

다 됐습니다! 이 위임 패턴이 어떻게 발전할 수 있는지에 대한 자세한 내용은 아래 토론을 참조하시오. 그렇지 않으면 [중간 커스텀 논리 — 종속성 주입](customlogicmiddle.md)로 이동하시오.

## <a name='function-based-generic-views'>토론: 함수 기반 일반 views</a>
Not yet, Comming Soon!


## <a name='going-further-with-generics'>토론: 제네릭으로 더 나아가기</a>
Not yet, Comming Soon!


## <a name='copy-paste-bad-reuse-good'>토론: 복사-붙여넣기 좋지 않음, 재사용 좋음?</a>
Not yet, Comming Soon!


## <a name='multiple-mixins'>토론: 다중 mixins</a>
Not yet, Comming Soon!



