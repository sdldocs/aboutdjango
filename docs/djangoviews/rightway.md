## <a name='pattern'>패턴</a>

Django에서 HTML 기반 view를 작성하는 방법은 다음과 같습니다.

```
from django.template.response import TemplateResponse

def example_view(request, arg):
    return TemplateResponse(request, 'example.html', {})
```

그리고 해당 url.py

```
from django.urls import path

from . import views

urlpatterns = [
    path('example/<str:arg>/', views.example_view, name='example_name'),
]
```

어떻게 변경할까요?

* `example_view`는 `home` 또는 `article_list`와 같이 view를 설명하는 함수 이름이어야 한다.
* `example.html`은 적용할 template의 경로이다. 저자는 이 가이드에서 template을 작성하는 방법에 대해서는 다루지 않으므로, 원한다면 [view 및 template에 대한 Django 튜토리얼](https://docs.djangoproject.com/en/stable/intro/tutorial03/)과 [Django template 언어 토픽](https://docs.djangoproject.com/en/stable/ref/templates/language/)을 참조하세요.
* `TemplateResponse`의 세 번째 인수인 `{}`는 template에서 사용할 수 있는 context 데이터이다.
* `arg`는 임의의 선택적 URL 매개 변수에 대한 위치 표시자이다. 즉, 경로 변환기(여기서 str을 사용)와 일치하고 매개 변수로 view 함수에 주어지는 URL 경로의 일부이다. 제거하거나 추가할 수 있지만 일치하도록 URLconf를 변경해야 한다.
* `urls.py`에서 인수를 `path`로 각각 변경한다.

    * (캡처된 부분을 포함하여) 일치하는 URL,
    * 위에 정의된 view 함수,
    * 그리고 `home` 또는 `myapp_articles_list`와 같이 프로젝트 전체에서 고유하며 [URL reversing](https://docs.djangoproject.com/en/stable/topics/http/urls/#reverse-resolution-of-urls)을 사용할 수 있는 선택적 이름이다.

이상이다.

그러나, 여러분이 Django views를 잘 사용하려면, 좀 더 깊은 이해가 필요하기 때문에, 설명을 위해, 이 페이지는 다른 부분보다 꽤 길다.

## <a name='explanation'>설명</a>
첫째, view가 무엇인지 아는 것이 중요하다. Django 문서에 따르면:

> view … 는 웹 요청을 받아 웹 응답을 반환하는 Python 함수이다.

정의를 보면, 가장 기본적인 view 함수는 무엇일까? "Hello World!" views.py에는 다음과 같은 기능이 있다.

```
from django.http import HttpResponse

def hello_world(request):
    return HttpResponse('Hello world!')
```

이 함수는 매개 변수로 'request' 객체를 수신해야 한다. 이는 사용자의 브라우저가 보낸 요청에 대한 모든 정보가 있는 HttpRequest의 인스턴스이다. 다음 HttpResponse 객체를 반환한다. 이는 사용자의 브라우저로 다시 전송되는 모든 데이터(일반적으로 웹 페이지인 HTTP 헤더 및 본문)가 포함된다. 위의 경우에는 Hello world!라는 문자열만 보냈다. 이 요청-응답 주기가 Django 웹 프레임워크의 핵심이다.

실제로 django가 우리의 view 함수을 호출하도록 하려면, 우리는 함수를 어딘가의 "URLconf"에 연결해야 합니다. [Django tutorial part 1](https://docs.djangoproject.com/en/stable/intro/tutorial01/#write-your-first-view)에서 이것을 설명하고 있어, 모든 앱 레이아웃에 대해 자세히 다루지 않을 것이다. 간단히 말해서, 우리는 이를 urls.py에 넣을 것이다.

```
from django.urls import path

from . import views

urlpatterns = [
    path('hello/', views.hello_world, name='hello_world'),
]
```

대부분의 경우, 단일 view 함수가 실제로 매개 변수로 사용된 URL 패밀리와 일치하고 view 함수에서 해당 매개 변수를 액세스하기 원한다. Django는 이것에 대한 내장된 지원을 가지고 있다. /hello/XXX/와 같은 URL을 일치시킨다고 가정한다. 여기서 XXX는 임의의 문자열이 될 수 있다. 그러면 URLconf는 다음과 같다.

```
urlpatterns = [
    path('hello/<str:my_arg>/', views.hello_world, name='hello_world'),
]
```

그리고 view 시그니처는 아래와 같다.

```
def hello_world(request, my_arg):
    # etc
```

이제, 우리는 고전적인 웹 앱을 위해, 일반적으로 HTML, 즉 웹 페이지를 제공할 수 있다. 또한 HTML에는 일반적으로 정적 웹 사이트가 아닌 동적 웹 사이트를 위해 추가할 부분이 있으며, HTML 이스케이프를 처리하고 서로 다른 페이지에 대한 공통 페이지 요소(예: 탐색)를 제공하는 순서대로 작성하려고 한다. 그래서 거의 모든 경우 Django의 template 엔진을 사용한다. [Django tutorial part 3](https://docs.djangoproject.com/en/stable/intro/tutorial03/#write-views-that-actually-do-something)에서 다루고 있다. `"Hello world"` 문자열을 전달하는 대신 `hello_world.html` template를 사용하여 페이지에 표시해야 하는 동적 정보인 "context data"를 전달한다.

그리하여 수정된 view는 아래와 같다.

```
from django.http import HttpResponse
from django.template import loader


def hello_world(request, my_arg):
    template = loader.get_template('hello_world.html')
    context = {}
    return HttpResponse(template.render(context, request))
```

template는 Django view의 필수적인 부분이 아니며 HTTP 요청과 응답이 필수적인 부분이다. template는 응답 본문을 작성하는 한 방법일 뿐이다. 하지만 이런 앱의 경우, template는 매우 흔하다. 따라서, Django 튜토리얼에서 언급했듯이, template를 로드하고, 렌더링하고, 응답하는 프로세스에 대한 shortcut [render()](https://spookylukey.github.io/django-views-the-right-way/the-pattern.html#:~:text=into%20a%20response%20%E2%80%94-,render(),-.%20With%20that%2C%20we) 함수가 있다. 이를 통해 다음과 같이 view를 요약할 수 있다.

```
from django.shortcuts import render


def hello_world(request, my_arg):
    return render(request, 'hello_world.html', {})
```

위의 예에서 세 번째 매개 변수는 컨텍스트 빈 딕셔너리(dictionary)이다.

이것은 view에서 사용하기 좋은 패턴이다. 그러나 Django에는 [TemplateResponse](https://docs.djangoproject.com/en/stable/ref/template-response/#templateresponse-objects)라는 다른 트릭이 있다.

`render`를 사용할 때의 문제는 template에서 가져온 메모리가 없는 일반 `HttpResponse` 객체를 다시 생성하는 것이다. 그러나 때때로 함수가 무엇으로 이루어졌는지를 나타내는 반환 값 "구성"이, 즉 원래 template와 context가 저장한 값을 반환하도록 하는 것이 유용하다. 이는 테스트에서 매우 유용할 수 있을뿐만 아니라, 최종적으로 '렌더링'되어 사용자에게 전송되기 전에 응답 내용을 확인하거나 변경하기 위해 view 함수 기능 이외의 다른 기능(예: 데코레이터 또는 미들웨어)을 원하는 경우에도 유용하다.

현재까지 `TemplateResponse`가 일반 `HttpResponse`보다 더 유용한 값을 반환한다. (이미 모든 곳에서 `render`를 사용한다면, 변경할 필요는 없으며 이 가이드내 거의 모든 경우에서 `render` 대신 `TemplateResponse`를 동일하게 작동한다.

이렇게 바꾸어 view로 시작하는 패턴에 도달했다.

```
from django.template.response import TemplateResponse

def example_view(request, arg):
    return TemplateResponse(request, 'example.html', {})
```

위에서 설명한 각 요소가 무엇인지 알아야 합다. **하지만 여기까지**. 다음 파트로 갈 수 있다. 또는 여기까지만 읽고 멈출 수도 있다. 이제 Django에서 HTML view를 작성하는 데 필요한 모든 것을 알게 되었다.

`TemplateView`, `ListView`, `DetailView`, `FormView`, `MultipleObjectMixin` 등 같은 CBV API와 모든 상속 트리 또는 메서드 흐름도를 공부할 필요가 없다. 그것들은 여러분의 프로그래밍을 더 힘들게 할 뿐이다. 그들의 문서를 인쇄하고 창고에 넣거나, 차라리 창고에 넣고 다이너마이트로 채우고 뒤를 돌아보지 마세요.

다음: [view에서 작업 수행](doanything.md)

## <a name='disussion'>토론: view를 볼 수 있도록 유지합시다! </a>
Not yet, Comming Soon!
