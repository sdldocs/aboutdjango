view를 작성할 때 일반적인 상황은 주 로직 이전에 view 시작 부분에서 수행해야 하는 일부 검사가 있으며 여러 view에서 동일한 검사를 공유할 수 있다. 검사가 실패하면 사용자를 다른 페이지로 redirect할 수 있지만 메시지 디스플레이([메시지 프레임워크](https://docs.djangoproject.com/en/stable/ref/contrib/messages/) 사용)와 같은 다른 옵션을 사용할 수 있다.

Python "decorator"는 이러한 종류의 요구사항과 완벽하게 일치한다.

만약 decorator를 전혀 사용하지 않았다면, 저자는 [Primer on Python Decorators](https://realpython.com/primer-on-python-decorators/) 읽기를 추천한다. 기존 decorator를 view에 적용하려는 경우에는 매우 간단하지만, 어떤 일이 일어나고 있는지 잘 이해하는 것은 view를 구현할 수 있으려면 꼭 필요하다. 게다가, 여러분은 이 매우 일반적인 Python 기술로부터 다른 방식으로 엄청난 이익을 얻을 것이다.

먼저 우리의 출발점을 살펴본다. 우리는 '프리미엄' 사용자만 접근할 수 있는 페이지를 가지고 있다. 프리미엄이 아닌 사용자가 페이지에 대한 링크를 받는 경우 계정 페이지로 redirect되고 메시지도 디스플레이된다.

다음과 같이 보일 수 있다.

```
def my_premium_page(request):
    if not request.user.is_premium:
        messages.info(request, "You need a premium account to access that page.")
        return HttpResponseRedirect(reverse('account'))
    return TemplateResponse(request, 'premium_page.html', {})
```

이제, 처음 세 줄의 논리를 다시 사용하기를 원하면, 가장 깔끔한 방법은 다음과 같이 사용할 decorator에 넣는 것이다.

```
@premium_required
def my_premium_page(request):
    return TemplateResponse(request, 'premium_page.html', {})
```

decorator 구현 방법을 이해하려면 decorator 구문이 수행하는 작업을 기억하는 것이 종종 유용하다. `my_premium_page`를 정의하는 일반적인 방법은 다음과 같다.

```
def my_premium_page(request):
    return TemplateResponse(request, 'premium_page.html', {})

my_premium_page = premium_required(my_premium_page)
```

즉, `premium_required`는 view 함수를 입력으로 사용하고, 새로운 대체 view 함수를 출력으로 반환하는 함수이다. 반환되는 view 함수는 원래 view 함수를 **wrapp** 한다. 이 경우에도 일부 검사와 로직을 추가하고, 경우에 따라(사용자가 premium 사용자가 아닌 경우) 원래 view 함수을 우회해 자체 응답을 반환하기로 결정한다.

따라서 `premium_required`의 구현은 다음과 같다.

```
import functools

def premium_required(view_func):

    @functools.wraps(view_func)
    def wrapper(request, *args, **kwargs):
        if not request.user.is_premium:
            messages.info(request, "You need a premium account to access that page.")
            return HttpResponseRedirect(reverse('account'))
        return view_func(request, *args, **kwargs)

    return wrapper
```

`@funtools.wraps(view_func)` 줄은 꼭 필요하지 않을 수도 있다. 그러나 wrapper 함수 view는 다른 애트리뷰트와 함께 원래 view의 이름과 docstring을 복사한다. 이들은 디버깅을 더 쉽게 만들고, 때로는 기능적으로도 중요할 수 있다 (예를 들어, `csrf_exempt`로 포장된 것을 wrap하는 경우). 따라서 항상 이줄을 추가해야 한다.

지금까지 사용하고 있는 view는 단일 `request`만 받기 때문에 wrapper가 `*args`와 `**kwrgs`를 가져가도록 하는 것은 필요하지 않아 보일 수 있다. 하지만 이 decorator가 일반적이고 미래의 증거가 되기를 원한다. 그래서 우리는 그것들을 처음부터 거기에 넣는다.

## <a name='adding-multiple-decorators'>여러 decorator 추가</a>
위와 같은 decorator에는 문제가 있다. 익명 사용자가 액세스하면 `request.user`는 `AnonymousUser` 인스턴스가 되고 `is_premium` 속성을 갖지 않으므로 `500` 오류가 발생한다.

이것을 해결하는 좋은 방법은 Django 제공 `login_required` decorator를 사용하는 것인데, 이는 익명 사용자를 위한 로그인 페이지로 redirect 된다. 단지 두 가지 decorator을 모두 적용하면 된다. 올바른 순서는 다음과 같다.

```
from django.contrib.auth.decorators import login_required

@login_required
@premium_required
def my_premium_page(request):
    return TemplateResponse(request, 'premium_page.html', {})
```

`login_required` 검사를 통해 `premium_required` view wrapper에 들어갈 때까지 로그인 사용자 임을 확인할 수 있다.

### decorator들의 순서
위와 같이 여러 decorator를 다룰 때는 순서가 매우 중요할 수 있으며, decorator들이 어떤 순서로 진행되는지 헷갈리기 쉽다.

저자가 아는 가장 좋은 비유는 그것을 **양파**로 생각하는 것이다. 중앙에는 실제 view 함수가 있고, 각 decorator는 레이어로 추가된다. 그것을 시각화로 길게 작성해 보자.

```
def my_premium_page(request):
    return TemplateResponse(request, 'premium_page.html', {})

my_premium_page = \
    login_required(
        premium_required(
            my_premium_page
        )
    )
```

그래서 `premium_required`는 가장 안쪽에 있는 decorator 이다. `my_premium_page`에 가장 먼저 적용되는 반면 `login_required`는 가장 바깥쪽 decorator이며 마지막으로 적용된다.

**하지만!** decorator 자체(`premium_required` 및 `login_required` 함수)는 반환하는 wrapper와 구별된다!

따라서 `login_required` wrapper가 추가하는 사전 조건이 가장 바깥쪽이기 때문에 **먼저** 실행되고 `premium_required` wrapper가 추가하는 preconditions이 가장 안쪽이기 때문에 **마지막**으로 실행된다.

각 decorator에 의해 추가된 preconditions은 소스 코드에 decorator가 나타나는 순서대로 실행된다.

그러나 view wrapper에서 후처리로 수행할 수도 있다. 양파 비유를 기억하시오. 가장 안쪽 wrapper의 후처리가 가장 바깥쪽 포장지의 후처리보다 먼저 실행된다.

### 연습
위의 내용이 여러분을 많이 혼란스럽게 만들었다면, 이것을 이해하는 가장 좋은 방법은 몇 가지 예를 들어 여러분에게 설명하는 것이 좋다고 생각한다. 그래서 여기 연습문제가 있다.

다음과 같은 decorator가 있다고 가정해 보겠다 (일부 작업을 인쇄하는 것뿐이며 아무것도 하지 않는다).

```
def decorator_1(view_func):
    print("In decorator_1")

    def wrapper(request, *args, **kwargs):
        print("In decorator_1 wrapper, pre-processing")
        response = view_func(request, *args, **kwargs)
        print("In decorator_1 wrapper, post-processing")
        return response

    return wrapper


def decorator_2(view_func):
    print("In decorator_2")

    def wrapper(request, *args, **kwargs):
        print("In decorator_2 wrapper, pre-processing")
        response = view_func(request, *args, **kwargs)
        print("In decorator_2 wrapper, post-processing")
        return response

    return wrapper
```

그러면 다음 코드 블록은 무엇을 인쇄할까?

```
>>> @decorator_1
... @decorator_2
... def my_view(request):
...     print("In my_view")
...     return "I am a response"
```

```
>>> response = my_view(None)
```

먼저 추측을 한 다음 Python 프롬프트에서 코드를 시도하시오. 만약 여러분이 틀렸다면, 여러분이 무슨 일이 일어나고 있는지 정확히 이해할 때까지 한 번 더 보시오.

힌트:

- @ 구문을 풀어 일반 버전으로 바꿉다.
- decorator를 사용하지 않고, decorator 하나를 사용한 다음, 두 개의 decorator를 사용하여 풀어보시오.

### decorator 결합
만약 여러분이 여러 decorator들을 특정 순서로 적용해야 하거나 종종 모두 함께 가지고 있어야 한다면, 여러분은 아마도 그것들을 결합하여 단일 decorator로 만드는 것을 생각해야 할 것이다. Adam Johnson이 작상한 [How to Combine Two Python Decorators!](https://adamj.eu/tech/2020/04/01/how-to-combine-two-python-decorators/)을 여러분에게 알려주는 것이 바람직하다고 생각한다.

또한 Stackoverflow 게시물 [임의의 여러 decorator들을 구성하기 위한 일반 코드](https://stackoverflow.com/questions/5409450/can-i-combine-two-decorators-into-a-single-one-in-python)를 참고할 수 있다.

## <a name='built-in-decorators'>내장 decorators</a>
또한, Django에서 제공하는 것보다 decorators와 "decorator favtories"을 놓치지 말고, `login_required`(이미 사용된), [user_passes_test](https://docs.djangoproject.com/en/stable/topics/auth/default/#django.contrib.auth.decorators.user_passes_test) 및 [permission_required](https://docs.djangoproject.com/en/stable/topics/auth/default/#the-permission-required-decorator)와 같은 많은 일반적인 사례를 알아 보시오.

다음: [정책 적용](applyingpolicies.md)

## <a name='mixins-do-not-compose'>토론: Mixins은 구성하지 않는다</a>
Not yet, Comming Soon!
