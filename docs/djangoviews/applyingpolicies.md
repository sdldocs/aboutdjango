view 그룹에 적용할 보안 정책과 같은 특정 정책이 필요할 수 있다. 예를 들어 정책이 `login_required`와 같은 decorator에 해당될 수 있으며 정책을 적용해야 하는 전체 모듈 또는 앱일 수 있다.

여러분이 잊지 않도록 FBV를 사용하여 처리하는 가장 좋은 방법은 무엇일까? 이 문제를 "종합적인 precondition"이라고 할 수도 있다. 이전의 [Precondition](preconditions.md) 패턴은 훌륭하지만 view에 적용하는 것을 잊으면 어떻게 될까?

좀 더 어렵게 하기 위해, 우리는 이 주제에 대한 몇 가지 변형이나 그것을 표현하는 대안을 가질 수 있다.

- 우리는 모듈에서 "하나 또는 두 개를 제외한 모든 view"에 적용하기를 원할 수 있다
- 또는 "구체적으로 제외하지 않는 한 기본적으로 모든 view"
- 또는 "각각 view에 N개 중 하나만 적용되어야 한다"
- 또는 "익명 액세스는 옵트인이어야 한다" (Django에서처럼 기본값이 아닌)

## <a name='solution-1'>Solution 1: Django-decorator-include</a>
[django-decorator-include](https://github.com/twidi/django-decorator-include)은 이 문제를 정확히 해결하는 깔끔한 작은 패키지이다. 이는 [include](https://docs.djangoproject.com/en/stable/ref/urls/#include)와 동일하게 작동하지만 포함된 모든 URL에 decorator를 적용한다.

이 패턴은 서드 퍼티 앱을 포함할 때 특히 유용하다. 코드를 건드리지 않고 단일 일괄 정책을 적용할 수 있다. 그러나 특히 자신의 코드일 때는 몇 가지 단점이 있다.

- URL 수준에서 작동하며, 원하는 것과 약간 차이가 있을 수 있다.
- 그것은 "분명히 옳지 않은" 여러분만의 view 함수를 남긴다. `login_required`로 decorate된 것으로 예상되는 view는 이제 비어 있으며, 보안이 다른 지점에 적용된다는 것을 기억해야 한다.

    더 나쁜 것은 이 패턴을 사용하지 않거나 사용할 수 없는 코드 기반의 일부가 있을 수 있다는 것이다. 그래서 여러분의 사고방식을 전환해야 한다. 만약 여러분이 decorator가 없는 view를 보게 된다면, 그것은 보안 문제인가 또는 아닌가? 여러분은 결국 진짜 문제를 무시하도록 잠재의식 훈련을 끝낼 수 있다, 이것은 꽤 좋지 않은 일이다.

- 그것은 예외를 처리하는 명백하고 쉬운 메커니즘을 가지고 있지 않다.

## <a name='solution-2'>Solution 2: 검사와 decorator include</a>
따라서 decorator에 보안 전제 조건을 추가하는 대신, 위의 `decorator_include`를 계속 사용하는 것이 위 기술의 수정된 버전이지만, 우리는 decorator가 이미 (가져올 때) 필수 decorator가 적용되었는지 확인하고 실행 시에는 아무것도 하지 않도록 한다.

검사 decorator는 다음과 같이 보일 수 있다.

```
_SECURITY_POLICY_APPLIED = "_SECURITY_POLICY_APPLIED"

def check_security_policy_applied(view_func):
    if not getattr(view_func, _SECURITY_POLICY_APPLIED, False):
        raise AssertionError(f"{view_func.__module__}.{view_func.__name__} needs to have a security policy applied")
    return view_func
```

(전체 코드 예제 - [decorators](https://github.com/spookylukey/django-views-the-right-way/tree/master/code/the_right_way/policies/decorators.py) 및 [URLs](https://github.com/spookylukey/django-views-the-right-way/blob/master/code/the_right_way/policies/urls.py#L18) 참조)

우리 decorator는 단순히 view 함수에서 보안 정책이 적용되었음을 나타내는 속성의 존재를 확인한다. 이 상수를 import하는 것을 지원하지 않으며 대신 정책을 적용하는 여러 decorators 중 하나를 통해 사용해야 함을 나타내기 위해 여기에 밑줄로 시작하는 상수를 사용하여 정의했다. 이전의 "premium required" 예를 사용하면 decorator 중 하나은 다음과 같이 보일 수 있다.

```
import functools
from django.contrib import messages
from django.http import HttpResponseRedirect


def premium_required(view_func):
    @functools.wraps(view_func)
    def wrapper(request, *args, **kwargs):
        if not (request.user.is_authenticated and request.user.is_premium):
            messages.info(request, "You need to be logged in to a premium account to access that page.")
            return HttpResponseRedirect('/')
        return view_func(request, *args, **kwargs)

    setattr(wrapper, _SECURITY_POLICY_APPLIED, True)
    return wrapper
```

이제 decorator로 `check_security_policy_applied`와 함께 `decorator_include`를 사용할 수 있습니다. 우리의 모든 view가 `@premium_required`로 decorate 되다면 모든 것이 순조로울 것이다. 그렇지 않으면 런타임이 아닌 import 때에 예외가 발생하므로 무시하거나 너무 늦게 알 수 없다.

(참고로, 이와 같은 것을 구현할 때는 실제로 실패할 것으로 예상되는 방식으로 실패하는지 다시 확인해야 한다.)

이 메커니즘은 매우 유연하며 일반 정책에 대한 예외를 허용하는 데 사용할 수 있다. 예를 들어 `anonymous_allowed` decorator를 추가할 수 있다.

```
def anonymous_allowed(view_func):
    @functools.wraps(view_func)
    def wrapper(request, *args, **kwargs):
        return view_func(request, *args, **kwargs)

    setattr(wrapper, _SECURITY_POLICY_APPLIED, True)
    return wrapper
```

이 decorator가 추가된 wrapper는 실제로 원래 view 함수로 전달될 뿐입니다. `_SECURITY_POLICY_APPLICED` 속성을 설정할 수 있도록 하기 위해서만 존재한다. 그러나 이것을 사용하면 view 함수에 대한 Django의 "기본적으로 모든 사람에게 개방" 정책에서 "기본적으로 비공개" 또는 원하는 다른 정책으로 성공적으로 전환할 수 있다.

`check_security_policy_applied`로 돌아가서 오류 메시지 목록을 가능하거나 선호하는 방법으로 수정한면 이 솔루션을 보다 친숙하게 만들 수 있다.

## <a name='solution-3'>Solution 3: 성찰(introspection)</a>
이전 솔루션의 나머지 문제는 URL 공간에 연결되어 있는 것이다. `decorator_include`를 사용하여 일부 URL을 응용프로그램에 추가할 때만 검사가 실행된다. 그것이 항상 우리가 바라는 것이 아닐 수도 있다.

대신 "all view functions everywhere"에 정책을 적용하거나 다른 사용자 정의 함수에 적용하는 것이 좋다. 이 경우 한 가지 해결책은 URLconf를 로드한 후 내부 검사를 수행하는 것이다. 자세한 내용은 정확히 수행하려는 작업에 따라 다르지만 [코드 폴더에 예제](https://github.com/spookylukey/django-views-the-right-way/blob/master/code/the_right_way/policies/introspection.py)가 있다. [Django system checks framework](https://docs.djangoproject.com/en/stable/topics/checks/)는 이러한 종류의 오류를 보고하는 데 좋은 옵션이며, 또는 이전과 같이 `raise AssertionError`을 사용하여 보다 적극적으로 대처할 수 있다.

이를 구현할 때 "all views within an app"와 같이 이 정책을 적용하려면 "within an app"이 무엇을 의미하는지 파악하는 것이 가장 어렵다. view 함수가 기존의 `views.py` 모듈 외부에서 정의되거나 완전히 다른 앱에서 import될 수 있다. 여러분의 성찰으로 이러한 경우들을 설명하고 여러분이 필요한 것을 하도록 하자!

다음: [Thin views](thinviews.md)

## <a name='secure-by-default'>토론: 기본 보안</a>
Not yet, Comming Soon!
