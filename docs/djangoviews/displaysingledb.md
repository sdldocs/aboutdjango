이 [예](urlparameters.md)를 계속하기 위해 URL의 일부인 product slug에서 개별 product 페이지를 검색하여 디스플레이하려고 한다.

이를 위해서는 `QuerySet`, 특히 [QuerySet.get](https://docs.djangoproject.com/en/stable/ref/models/querysets/#django.db.models.query.QuerySet.get) 메서드의 사용 방법을 알아야 한다.  slug라는 이름의 [SlugField](https://docs.djangoproject.com/en/stable/ref/models/fields/#slugfield)에 `Product` 모델이 있다고 가정하면 코드는 다음과 같다.

```
product = Product.objects.get(slug='some-product-slug')
```

그러나 이 경우 `Product.Not Exist 예외가 발생한다. 이 예외를 catch 해야 한다. crash 대신, 여러분은 사용자에게 404 페이지를 보여주어야 한다. 여러분은 django의 [Http404](https://docs.djangoproject.com/en/stable/topics/http/views/#django.http.Http404) 예외를 사용하여 이를 쉽게 처리할 수 있다.

수정된 코드는 다음과 같다.

```
try:
    product = Product.objects.get(slug=slug)
except Product.DoesNotExist:
    raise Http404("Product not found.")
```

이것이 여러분이 어떤 식으로든 당황해서는 안되는 완벽하고 적절한 코드이다. 그러나 이 패턴은 Django 앱에서 너무 자주 나타나므로 [get_object_or_404](https://docs.djangoproject.com/en/stable/topics/http/shortcuts/#get-object-or-404)라는 바로 가기(shortcut)가 있다. 위의 논리를 결합하여 다음과 같이 작성할 수 있다.

```
# imports
from django.shortcuts import get_object_or_404

# in the view somewhere
product = get_object_or_404(Product.objects.all(), slug=slug)
```

product 객체를 template으로 렌더링하는 것이 유일한 작업인 경우 view의 간결한 최종 버전은 다음과 같다.

```
def product_detail(request, slug):
    return TemplateResponse(request, 'shop/product_detail.html', {
        'product': get_object_or_404(Product.objects.all(), slug=slug),
    })
```

다음: [객체 리스트 디스플레이](displaylists.md)

## <a name='layering-violations'>토론: 위반 계층화 - shortcut vs mixins</a>
Not yet, Comming Soon!


## <a name='comparison-to-detailview'>토론: Detail View와 비교</a>
Not yet, Comming Soon!


## <a name='convention-vs-configuration'>토론: 컨벤션 대 설정</a>
Not yet, Comming Soon!


## <a name='static-vs-dynamic'>토론: 정적 대 동적</a>
Not yet, Comming Soon!


## <a name='generic-code-and-variable-names'>토론: 정적 대 동적</a>
Not yet, Comming Soon!

