Django를 사용하는 개발자에게 유익한 정보를 제공하고자 하는 바람에서 [Django Views - The Right Way](https://spookylukey.github.io/django-views-the-right-way/index.html)를 편역한 것임을 밝힙니다.

Django에서 views를 작성하는 방법에 대한 저자의 고집스러운(opinionated) 가이드에 오신 것을 환영합니다!

이는 저자가 15년 이상에 걸쳐 다양한 Django와 파이썬 프로젝트에서 했던 실수와 얻은 교훈의 결과물이다.

'함수 기반 Views'(FBV, Function Based Views)로 개발하는 것조차 두려워할 정도인 일부 개발자들에게 '클래스 기반 Views'(지금부터 CBV, Class Based Views)로 Django views를 가르치고, 그들이 배우는 것이 필수가 되었다는 사실이 계기가 되었다.

최악의 경우는 아마도 일부 공식 Django 문서에서 [mixins의 계속되는 어려움에 도움을 줄 수 있는 조언](https://docs.djangoproject.com/en/stable/topics/class-based-views/mixins/)을 하였지만, 실제로 개발자는 어려워 하고, 어려움에서 벗어나기 어려웠다. 일부 공식 Django 문서에서의 mixins 어려움을 줄일 수 있는 데 도움이 될 선의의 조언을 했지만, 실제로 개발자들은 어려워하고 그들을 어려움에서 벗어나게 하지는 못했다("git 비난"을 조금 한 후에 그 페이지에 대한 커밋 로그에 기록된 것의 저자임이 [밝혀졌다](https://github.com/django/django/commit/c4c7fbcc0d9264beb931b45969fc0d8d655c4f83). 저자는 그런 일이 일어나는 것이 싫었다).

그래서, 이 모든 것을 고려하여, 저자는 개발자의 시간을 절약하고, 적절한 길을 보여주기 위해 이 글을 작성하게 되였다:-)

FBV는 매우 쉽고 간단하기 때문에 이 가이드의 핵심 부분은 매우 짧다. 사실, 뷰를 위한 Django 튜토리얼은 이미 당신이 알아야 할 모든 것을 설명하고 있다. 가볍게 읽으며 CBV에 대한 부분은 건너뛰는 것이 나을 수도 있다.

하지만 같은 것에 대해 다른 견해를 갖고 싶다면, 이 글이 독자를 위한 것일 수 있다. 저자는 또한 CBV가 종종 해결책으로 제안되는 FBV의 일반적인 작업과 패턴을 위하여 약간 추가하였다. 저자는 몇 가지 목표를 갖고 있다.

- views가 얼마나 간단하고 쉬울 수 있는지 보여주고 싶었다.
- 개발자의 삶을 더 힘들게 만들 뿐만 아니라 나쁜 패턴을 가르치는 추가 API 스택을 배우는 것으로부터 우리가 자유로워지기를 바랬다.
- Django 특정 API를 많이 배우는 대신, 훨씬 더 많은 설명 가능한 지식을, 예를 들면 HTTP 원칙, 일반적인 OOP/다중 패러다임 프로그래밍 원리, 일반적인 파이썬 기술 등을 다루고 싶었다.

또한 view 함수에 대한 모든 URL 매개 변수의 입력을 확인하는 방법과 같은 다른 장점도 있습니다.

다른 두 종류의 독자를 위하여, 각 페이지는 두 부분으로 구성되어 있다. 

첫째, 작업 - 무엇을 어떻게 해야 할지 즉 적절한 방법에 대한 짧고 확실한 가이드이다. 일반 개발자 또는 Django에 대한 지식 측면에서 경험이 적은 개발자를 위한 것으로 그들은 이 부분만 읽는 것으로 충분하다. 이 가이드는 참조 문서가 아니기 때문에 Django 공식 참조 문서에 대한 다양한 링크를 제공하고 았으며 예제 코드 모두 GitHub repo에 저장되고 공개되어 있다.

둘째, 토론 - 왜 여러분에게 달리 말하는 사람들이 틀리는 지에 대한 더 길고 깊이 있는 설명이다 :-) 조금 더 경험이 많은 개발자들, 특히 사람들을 가르치거나 코드 베이스에 사용되는 패턴에 대한 결정을 내리는 사람들을 대상으로 한다. 이 토론 섹션은 프로그래밍 일반 원칙, 파이썬과 Django에 적용되는 방법에 관한 것이다.

시작해요!

목차

- [적절한 방식](rightway.md)

    - [패턴](rightway.md#pattern)
    - [설명](rightway.md#explanation)
    - [토론: view를 볼 수 있도록 유지합시다!](rightway.md#disussion) 

- [view에서 작업 수행](doanything.md)
    - [토론: 출발점](doanything.md#discussion)


- [template에 데이터 추가](adddata.md)
    - [토론: 창피할 정도로 간단해?](adddata.md#embarrassingly-simple)
    - [토론: 표준 문안](adddata.md#boilerplate)

- [공통 context 데이터](commoncontext.md)
    - [토론: Helpers vs mixins](commoncontext.md#discussion)

- [view내 URL 파라미터](urlparameters.md)
    - [토론: 일반 코드와 함수 시그니처](urlparameters.md#generic-code-and-function-signatures)
    - [토론: 타입 확인 시그니처](urlparameters.md#type-checked-parameters)

- [단일 데이터베이스 객체 디스플레이](displaysingledb.md)
    - [토론: 위반 계층화 - shortcut vs mixins](displaysingledb.md#layering-violations)
    - [토론: Detail View와 비교](displaysingledb.md#comparison-to-detailview)
    - [토론: 컨벤션 대 설정](displaysingledb.md#convention-vs-configuration)
    - [토론: 정적 대 동적](displaysingledb.md#static-vs-dynamic)
    - [토론: 일반 코드와 변수 이름](displaysingledb.md#generic-code-and-variable-names)

- [객체 리스트 디스플레이](displaylists.md)
    - [토론: 재사용 가능한 코드 단위 찾기](displaylists.md#discovering-reusable-units-of-code)

- [시작 커스텀 논리 — 위임](customlogicstart.md)
    - [토론: 함수 기반 일반 views](customlogicstart.md#function-based-generic-views)
    - [토론: 제네릭으로 더 나아가기](customlogicstart.md#going-further-with-generics)
    - [토론: 복사-붙여넣기 좋지 않음, 재사용 좋음?](customlogicstart.md#copy-paste-bad-reuse-good)
    - [토론: 다중 mixins](customlogicstart.md#multiple-mixins)

- [중간 커스텀 논리 — 종속성 주입](customlogicmiddle.md)
    - [Notes: 용어(terminology)](customlogicmiddle.md#terminology)
    - [토론: DI vs 상속](customlogicmiddle.md#di-vs-inheritance)

- [Redirects](redirects.md)
    - [토론: urls.py에서 CBV 설정](redirects.md#cbv_configuration)
    - [토론: urls.py에서 FBV 설정](redirects.md#fbv_configuration)

- [Forms](forms.md)
    - [토론: 복잡한 form cases](forms.md#complex-form-cases)

- [Preconditions](preconditions.md)
    - [여러 decorator 추가](preconditions.md#adding-multiple-decorators)
    - [내장 decorators](preconditions.md#built-in-decorators)
    - [토론: Mixins은 구성하지 않는다](preconditions#mixins-do-not-compose)

- [Policy 적용](applyingpolicies.md)
    - [Solution 1: Django-decorator-include](applyingpolicies.md#solution-1)
    - [Solution 2: 검사와 decorator include](applyingpolicies.md#solution-2)
    - [Solution 3: 내성(introspection)](applyingpolicies.md#solution-3)
    - [토론: 기본 보안](applyingpolicies.md#secure-by-default)

- [Thin views](thinviews.md)



뭐 빠진 것이 있나요? 이 가이드는 작성 중이며, 아무리 추가해도 아마 항상 작성 중인 상태일 것이다! 독자가 다루어지길 원하는 토픽에 대한 요청이 있으면 [GitHub](https://github.com/spookylukey/django-views-the-right-way)에 이를 요창할 수ㄴ 있다.

## 주의사항 및 면책사항 등
1. 사실 적절한 방법은 하나가 아닐 수 있다. 그러나 이 가이드에는 없다!
2. 독자가 '클래식' 웹 앱이나 웹 사이트를 작성하고 있다고 가정한다. 이 사이트에서 여러분 페이지의 대부분은 서버에서 렌더링된 HTML이며, 아마도 일부 자바스크립트가 페이지에 로드될 수고 있다. 이 사이트는 여러분의 서버가 페이지를 하나로 묶는 클라이언트 측 자바스크립트 웹 앱으로 대부분 데이터(예: JSON)를 보내는 사이트가 아니라고 가정한다.
3. 제안하는 의견은 주로 Django와 함께 제공되는 CBV에 적용된다. 구체적으로, 비판 중 많은 부분은 Django Rest 프레임워크, (CBV의 형태를 사용하는)Django admin과 다른 구현에 적용되지 않는다. 이에 대한 자세한 내용은 이후 토론을 참조하세요.
4. 저자는 Django 코어 개발자이지만, 모든 Django 개발자들을 대변하는 것은 아니다. 사실 저자는 CBV가 처음 Django에 추가되었을 때 주변에 있었고, 심지어 그것들의 디자인에 조금 관여하기도 했다. 그 당시에는 저자는 지금 다루고 있는 것들을 알지 못했다. 그러므로 저자의 비판을 공격이 아닌 "학습 과정 에서의 논쟁"의 한 형태로 이해해 주갈 바란다.
5. 저자는 파이썬 3.9 이상과 Django "최근" 버전(작성 당시 3.2 이상)을 사용하고 있다고 가정한다. 그렇지만 거의 모든 것이 이전 버전(또는 최신 버전)에도 적용될 수 있다.

