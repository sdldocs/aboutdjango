[Django tutorial for views](https://docs.djangoproject.com/en/stable/intro/tutorial03/)와 [request handling docs](https://docs.djangoproject.com/en/stable/topics/http/urls/#how-django-processes-a-request)에 설명되어 있듯이 view 함수에 사용되는 URL의 일부를 캡처하고 싶다면 URL을 설정하여 수행할 수 있다.

개별 페이지에 product를 디스플레이하고자 하는 전자 상거래 사이트가 있다고 가정해 보자. 여러분은 `/product/`가 이 모든 페이지의 접두어가 되고, 다음 부분은 URL 친화적인 이름(예: “White T-Shirt” 대신 `white-t-shirt`)이 사용되길 원한다면 다음과 같이 수행할 수 있다.

```
# urls.py

from django.urls import path

from . import views

urlpatterns = [
    path('products/<slug:slug>/', views.product_detail, name='product_detail'),
]
```

```
# views.py

def product_detail(request, slug):
    return TemplateResponse(request, 'shop/product_detail.html', {})
```

URL conf에서 뿐만 아니라 view 함수 시그니처에 `slug` 매개 변수를 추가하는 방법에 주목하시오. URL 패턴에서, 첫 `slug`는 경로 변환기 타입이다. 두 번째 `slug`는 `product_detail` view에 있는 파라미터의 이름이고, 여러분은 다른 것을 선택하여 작성할 수도 있었다.

만약 여러분이 view를 이렇게 수정하지 않는다면, 당연히 작동하지 않고 예외를 발생시킬 것이다. 왜냐하면 Django가 여러분의 함수가 받아들이지 않는 매개 변수로 함수를 호출하려고 하기 때문이다. 이것은 Python이 발생시키는 오류이다.

CBV에 익숙하다면, 이것은 더 명백한 차이점 중 하나이다. CBV의 경우 수정할 함수 시그니처가 없으므로 함수 시그니처를 수정할 필요가 없다. 하지만 CBV를 사용하면 해당 매개 변수를 얻기 위해 더 많은 코드를 작성해야 한다.

URL에 추가할 수 있는 다양한 방법에 대해서 [path converter](https://docs.djangoproject.com/en/stable/topics/http/urls/#path-converters)에 대한 Django 문서를 확인하시오. 타입 힌트에 관심이 있는 경우 이 패턴을 개선하는 방법에 대한 팁도 아래를 참조하십시오.

그렇지 않으면 [단일 데이터베이스 객체 디스플레이](displaysingledb.md)로 이동한다. 여기서 실제로 `slug` 매개 변수를 사용한다.

## <a name='generic-code-and-function-signatures'>토론: 일반 코드와 함수 시그니처</a>
장고의 URL-to-function 발송 메커니즘은 매우 우아하고 강력하며, URL의 일부를 사용 준비가 완료되고 대기 중인 함수 매개 변수로 변환한다.

2.0에 경로 변환기 기능이 추가되면서 훨씬 편리해졌다. 왜냐하면 자동으로 적절한 타입으로 변환되어 함수에서 수행해야 하는 타입 변환의 짐이 줄어들기 때문이다.

다음과 같은 타입의 힌트를 추가하여 사용할 수 있다.

```python
def product_detail(request, name: str):
    pass  # etc

# OR
def product_detail(request, pk: int):
    pass  # etc
```

이제 인수 타입을 확인하기 위해 URLconf를 더 이상 확인할 필요가 없다. URLconf는 여러분의 함수에 있다.

함수 시그네이처의 일부로서 URL 매개 변수의 깔끔함은 매개 변수를 갖기 위해 추가 작업을 수행해야 하는 CBV와 대조된다.

```python
    name = self.kwargs['name']
```

## <a name='type-checked-parameters'>토론: 타입 확인 시그니처</a>
물론 타입 힌트를 추가하면 URL 설정이 인수의 이름과 타입 모두에서 뷰 함수와 일치하는지 자동으로 확인할 수 있다면 더욱 쿨하지 않을까?

[django-urlconfchecks](https://github.com/AliSayyah/django-urlconfchecks/)는 정확히 그렇게 할 것이다!

안타깝게도 CBV를 사용할 경우 여기서 이렇게 할 수 없다. 왜냐하면 타입 힌트로 나타낼 수 있는 시그네이처가 없기 때문이다. 외부적으로 보이는 여러분 뷰의 시그니처는 `view(request, *args, **kwrgs)`이므로 위의 타입체크(type-checking) 코드가 동작하는 것이 불가능하다.

여기서 근본적인 문제는 **generic 코드**이다. Generic 코드는 일반덕이기 때문에 다양한 상황에서 사용할 수 있다. 그러나 generic 코드의 단점은 여러분 자신만의 코드가 아니라 모든 상황을 충족해야 한다는 것이다. 그래서 CBV들은 당신이 원하는 것이 아닌 `kwargs` 사전을 가지고 있어야 한다. 정의상 generic 코드는 개인적인 터치가 부족하다.

물론 단점을 능가하는 장점이 있을 수 있다. 하지만 여러분이 놓치고 있는 것이 무엇인지 확실히 알고 있어야 한다.
