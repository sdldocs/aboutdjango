Django에서 redirect를 구현하려면 HTTP에서 redirect가 어떻게 작동하는지 알아야 한다.

HTTP에서 redirect는 300-399 범위의 상태 코드와 브라우저에 이동할 URL을 알려주는 위치 헤더를 가진 HTTP response이다. 여러분의 view에서 이와 같은 응답을 반환하면 브라우저는 지정된 URL에 대해 즉시 다른 요청을 수행한다.

[3XX 코드마다 의미가 달라](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#3xx_Redirection) 올바른 코드를 사용해야 한다.

그것은 HTTP 수준에서 당신이 알아야 할 것의 95%이다. Django에서 가장 일반적인 기능은 [HttpResponseRedirect](https://docs.djangoproject.com/en/stable/ref/request-response/#django.http.HttpResponseRedirect)에서 정리되어 있다.

예를 들어, 이 view는 무조건적이고 일시적인 redirect를 수행한다.

```
def my_view(request):
    return HttpResponseRedirect('/other/url/', status=307)
```

또한 Django는 다음과 같은 몇 가지 shortcut을 제공한다.

- [redirect](https://docs.djangoproject.com/en/stable/topics/http/shortcuts/#redirect) - HTTP 응답 개체를 반환하고 명명된 view와 기타 항목으로 redirect하기 위한 논리가 기본 제공되는 유틸리티이다.

- [RedirectView](https://docs.djangoproject.com/en/stable/ref/class-based-views/base/#redirectview) - redirect를 수행하는 전체 view를 제공하는 클래스로, 경로로 부터 인수를 포함하여 이름별로 view를 조회하고 쿼리 문자열을 복사할 수 있는 등의 몇 가지 깔끔한 기능이 있다. 보기에서 redirect만 수행하는 경우 이 옵션을 사용하는 것이 좋다. 그렇지 않으면 `HttpResponse` 객체를 직접 사용하시오.

    예를 들어 `/old-path/<number>/`에 이전 URL이 있고 이 URL을 `/new-path/<number>/`로 영구적으로 리디렉션하려는 경우 `urls.py`에서 아래과 같이 사용할 수 있다.

```
urls = [
    path('old-path/<int:pk>/', RedirectView.as_view(
        pattern_name='my_view',
        permanent=True,
        query_string=True,
    ),
    path('new-path/<int:pk>/', views.my_view, name='my_view'),
]
```

이상이다. [Forms](forms.md)로

## <a name='cbv_configuration'>토론: urls.py에서 CBV 설정</a>
Not yet, Comming Soon!


## <a name='fbv_configuration'>토론: urls.py에서 FBV 설정</a>
Not yet, Comming Soon!


