template에 사용할 데이터가 있다고 가정하자. 그러므로 그 데이터를 template의 "context"에 전달할 필요가 있다. 단순 값 또는 ORM을 사용하여 검색된 객체 목록과 같은 모든 항목이 될 수 있다. 앞에서 설명한 [패턴](https://spookylukey.github.io/django-views-the-right-way/the-pattern.html#the-pattern)을 사용하여 어떻게 해야 할까?

설명을 위해, 오늘 날짜를 today 변수로 context에 넣을 것이고, 저자는 여러분이 여러분의 사이트에 대한 홈페이지 보기를 작성하고 있다고 가정한다.

이미 말했듯이, view에서 무엇이든 할 수 있는 방법에 대한 대답은 "Just do it" 입니다.

```
from datetime import date

def home(request):
    return TemplateResponse(request, "home.html", {
        'today': date.today(),   # This is the line you add
    })
```

> **Note:** <br>
> **포맷팅:** 저자는 PEP8에 맞춰 코드 예제를 포맷하고 있으며, 그 이후에는 특히 변화된 것들을 강조하기 위해 특별히 포맷하였다. 이 예에서는 패턴에 한 줄을 추가하고 그에 따라 포맷했다. 형식을 따를 필요는 없으며, 여러분(또는 도구)이 다르게 할 수도 있다!
>
> **Import:** 간단히 말해서, 저자는 이미 언급했듯이 `import` 문을 생략한다. 소스 코드 전체를 원한다면 [코드 폴더](https://github.com/spookylukey/django-views-the-right-way/tree/master/code)를 참조하세요.

대부분 날짜 필터를 사용하지만 template에 따라 날짜 형식을 지정할 수 있으므로 문자열 대신 날짜 객체를 사용했다. 패턴에는 이미 빈 딕셔너리 context가 있고, 채워지기를 기다리고 있었고 바로 그 값을 넣을 수 있었다.

변형이 있을 수 있는데, 특히 조건에 따라 데이터를 추가하는 경우 context 데이터를 먼저 변수로 만드는 것이 도움이 되는 경우가 있다.

```
def home(request):
    today = date.today()
    context = {
        'today': today,
    }
    if today.weekday() == 0:
        context['special_message'] = 'Happy Monday!'
    return TemplateResponse(request, "home.html", context)
```

다음: [공통 context 데이터](commoncontext.md)

## <a name='embarrassingly-simple'>토론: 창피할 정도로 간단해?</a>
Not yet, Comming Soon!


## <a name='boilerplate'>토론: 표준 문안</a>
Not yet, Comming Soon!
