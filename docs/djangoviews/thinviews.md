이 페이지는 저자 가이드의 마지막 부분으로, view에 넣지 말아야 할 것에 관한 것이다.

저자의 기본 철학에 따르면 view는 다음과 같아야 한다.

- 수신 HTTP 요청 처리
- 발신 HTTP 요청 생성
- 다른 곳의 논리를 충분히 참고하여 이들을 결합

그리고 다른 일은 많이 하지 않도록 하자. 결과적으로 당신의 견해는 꽤 단순하고 논리적이지 않은 경향이 있을 수 있다. 이를 종종 "fat models, skinny views/controllers"라고 부르지만, 여기서는 view에만 초점을 맞춘다.

그것을 보는 또 다른 방법은 웹사이트에 전원을 공급하는 것뿐만 아니라 당신의 코드가 다른 방식으로 사용될 것이라고 상상하는 것이다. 여기에는 데스크톱 GUI, command line 앱 또는 사용자의 명령없이 자동으로 실행되는 예약된 작업의 일부가 포함될 수 있다. 그런 다음 웹 사이트와 다른 유형의 응용 프로그램 모두에서 공통적인 논리를 찾는다. 일반적인 로직은 view 함수 또는 view 레이어 유틸리티의 일부가 되어서는 안된다.

이를 설명하기 위해 몇 가지 예를 살펴보겠다.

## <a name='push-actions'>Example: 모델 계층에 푸시 액션</a>
이 예는 예약 시스템(항상 하지 않는 방법의 예를 찾을 수 있는 유익한 장소)을 위해 저자가 작성한 코드에서 시작한다. 장소 세부 정보를 바구니에 추가한 후 "Book now" 또는 "Put on shelf"을 선택할 수 있다.

간략한 view 코드는 다음과 같다.

```
def view_booking(request, pk):
    booking = request.user.bookings.get(id=pk)

    if request.method == 'POST':
        if 'shelve' in request.POST:  # pressed the 'Put on shelf' button
            booking.shelved = True
            booking.save()
            messages.info(request, 'Booking moved to shelf')

   # the rest...
```

이 코드의 문제는 view가 "putting on the shelf"이 무엇을 의미하는지에 대해 너무 많은 지식을 가지고 있다는 것이다. 미래에는 부울 `shelved` 속성을 사용하지 않을 수도 있지만, 다중값 플래그를 사용하거나 완전히 다른 것으로 사용될 수도 있다. 다른 스키마를 사용하면 저장해야 하는 다른 객체나 다른 작업이 될 수 있다. 우리는 이 논리가 한 곳에 있기를 원한다. 그래서 그것이 별로 신경 쓰지 않는 세부 사항으로 view를 복잡하게 만들지 않도록 우리 코드의 다른 부분이 같은 일을 할 때 항상 올바르게 사용될 수 있도록 한다. 

따라서 다음과 같은 코드 대신

```
booking.shelved = True
booking.save()
```

우리는 아래처럼 작성하여야 한다.

```
booking.put_on_shelf()
```

그러면 `shelved` 속성 또는 기타 수행해야 하는 작업을 처리하는 `Booking.put_on_shelf()` 메서드의 책임이 된다.

이것은 매우 간단한 예이며, 크게 달라 보이지 않을 수도 있다. 하지만 만약 여러분이 이런 종류의 논리를 view 레이어 밖으로 옮기는 습관이 생긴다면, 그것은 많은 도움이 될 것이다.

`messages.info()` 호출을 모델 계층으로 이동하지 **않았다**. 메시지를 웹 페이지에 디스플레이하는 것과 관련이 있으므로 메시지가 속한 view 계층에 머무른다.

## <a name='push-filtering'>Example: 모델 계층에 푸시 필터링</a>
위의 예를 계속 사용하여 예약 목록을 사용자에게 표시할 때 다른 유형의 필터링을 적용할 수 있다. 예를 들어, "in the basket" 예약, (위와 같은) "on the shelf" 예약 또는 "confirmed for this year" 예약을 표시를 원할 수 있다. 확정된 예약은 적어도 현재는 다른 부울 플래그로 제어된다.

다음과 같은 view 함수을 사용하여 이 필터링을 수행할 수 있다.

```
# In the basket
Booking.objects.filter(shelved=False, confirmed=False)

# On the shelf
Booking.objects.filter(shelved=True, confirmed=False)

# Confirmed for this year
Booking.objects.filter(confirmed=True, start_date__year=date.today().year)
```

그러나 이전처럼 스키마에 대한 정보를 view에 직접 너무 많이 입력한다. 이것은 몇 가지 나쁜 영향을 미친다.

- 우리가 한 곳 이상에서 그것을 원한다면 우리는 그 논리를 복제해야 할 것이다.
- 우리가 스키마를 바꾸면 우리는 이를 사용하는 모든 코드를 변경해야 할 것이다.
- 우리의 코드는 읽기 쉽지 않고, 우리는 그 필터링 조건들이 실제로 무엇을 가리키는지 알아내야 할 것이다. 위의 코드와 같이 각 코드에 대해 주석을 추가할 수 있다. 하지만 나는 항상 그런 댓글을 "code smells"로 해석하려고 노력한다. 그것들은 내 코드 자체가 명확하지 않다는 것을 알려주는 힌트이다.

[using filter directly in view code is a usually an anti-pattern](https://www.dabapps.com/blog/higher-level-query-api-django-orm/)이라는 Jamie Matthews의 의견에 저자는 동의한다. 이제 이러한 힌트를 듣고 코드를 변경하여 더 이상 주석이 필요하지 않도록 하겠다.

```
Booking.objects.in_basket()

Booking.objects.on_shelf()

Booking.objects.confirmed().for_year(date.today().year)
```

또한 다음과 같은 사용자 객체에서 동일한 기능을 사용할 수 있기를 바란다.

```
user = request.user
context = {
    'basket_bookings': user.bookings.in_basket()
}
# etc.
```

만약 관련된 사용자가 있다면, 저자는 보통 이렇게 생긴 코드를 선호한다. 목록을 표시하든 단일 항목을 검색하든 `user`와 함께 모든 사용자 관련 쿼리를 시작하는 습관을 들이면 접근 제어를 추가하는 것을 잊을 수 없으므로 [안전하지 않은 직접 개체 참조 보안 문제](https://portswigger.net/web-security/access-control/idor)가 발생할 가능성이 줄어든다.

이제 문제는, 우리가 어떻게 그런 인터페이스를 만들 수 있을까 하는 것이다.

### 연결 가능한 사용자 지정 QuerySet 메소드
`_basket()`, `on_shelf()`, `confirmed()`, `for_year()` 등을 사용자 지정 `QuerySet` 메소드으로 정의하는 것이 답이다. 단순히 `Manager` 메소드가 아닌 `QuerySet` 메소드를 만들면 위와 같이 체인화할 수 있으므로 확인 후 `_year()`를 사용하거나 다른 메서드를 사용할 수 있다.

[Django docs for QuerySets and Managers](https://docs.djangoproject.com/en/stable/topics/db/managers/)에서 방법을 보여주지만 `Manager/QuerySet` 분할로 인해 다소 부담스러울 수 있다. 기본 패턴은 다음과 같다.

```
class BookingQuerySet(models.QuerySet):
    # Custom, chainable methods added here, which will
    # do lower level 'filter', 'order_by' etc.
    def in_basket(self):
        return self.filter(shelved=False, confirmed=False)

    def for_year(self, year):
        return self.filter(start_date__year=year)

    # etc.


class Booking(models.Model):
    # fields etc

    objects = BookingQuerySet.as_manager()
```

`QuerySet` 인터페이스의 일부가 아닌 다른 메서드와 함께 사용자 지정 `Manager` 클래스를 추가로 원하는 경우 [Manager.from_queryset](https://docs.djangoproject.com/en/stable/topics/db/managers/#from-queryset)를 사용할 수 있다.

이 패턴을 최대한 활용하려면 [all the cool things that QuerySet can do](https://docs.djangoproject.com/en/stable/ref/models/querysets)을 알고 있어야 합니다. 예를 들어, 이 코드는 shelf나 basket에 있는 모든 것을 포함하는 `QuerySet`를 구성한다.

```
on_shelf_or_in_basket = Booking.objects.in_basket() | Booking.objects.on_shelf()
```

쿼리를 실행하지 않고 새 `QuerySet`가 생성된다. `_shelf_or_in_basket`를 수행할 때 두 가지 유형의 예약을 모두 반환하는 단일 DB 쿼리를 실행한다. 그래서 우리는 읽기 쉽고 스키마 정보를 부적절하게 유출하지 않는 효율적인 코드를 얻을 수 있다.

### 코드를 넣을 위치
view에 없는 경우 이 코드는 실제로 어디에 있을까? 만약 여러분이 위와 같이 "fat model"을 원한다면, 이것은 종종 `models.py` 파일에 저장된다.

하지만 다음 사항에 유의해야 한다.

- `models.py` 파일을 여러 모듈로 분할할 수 있다. 큰 파일을 만들 필요가 없다!
- 모델 계층 코드는 꼭 "database models"을 참조할 필요가 없다. 우리는 여기서 종종 Django 데이터베이스 모델이 직접 백업할 수 있는 "domain models"에 대해 이야기하고 있지만, 그것은 다른 클래스나 함수일 수 있다.
- Django `Model`과 관련된 모든 논리를 해당 클래스의 메서드에 넣을 필요는 없다. “listen to the code”, 그리고 비즈니스 레벨 요구사항을 듣고, 프로젝트에 적합한 개념을 찾고, 분할 하여야 한다.

### 끝
(항상 그렇듯이 아래의 토론 섹션을 제외하고) 가이드는 여기까지! 도움이 되었기를 바랍니다. 저자가 다루지 않은 일반적인 사항이 있다면 [GitHub에 언제든지 이슈로 올려주세요](https://github.com/spookylukey/django-views-the-right-way).


## <a name='service-layer'>토론: 서비스 계층?</a>
Not yet, Comming Soon!

## <a name='pragmatism-and-purity'>토론: 실용주의와 순수성</a>
Not yet, Comming Soon!
