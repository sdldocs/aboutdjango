전자 상거래 사이트의 예를 계속하기 위해, product 목록 페이지를 구현해 봅시다. 첫 번째 버전의 코드는 다음과 같다.

```
def product_list(request):
    return TemplateResponse(request, 'shop/product_list.html', {
        'products': Product.objects.all(),
    })
```

전형적인 product 목록 페이지는 일반적으로 최소한 페이지 구분을 포함하여 몇 가지 더 많은 요구 사항이 있다. Django는 유용한 [Paginator](https://docs.djangoproject.com/en/stable/topics/pagination/#using-paginator-in-a-view-function) 클래스를 지원하며, 사용 방법을 보여주는 유용한 문서를 제공한다. 추가하여 다음과 같은 기능도 제공된다.

```
from django.core.paginator import Paginator

def product_list(request):
    products = Product.objects.all()
    paginator = Paginator(products, 5)  # Show 5 products per page.
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return TemplateResponse(request, 'shop/product_list.html', {
        'page_obj': page_obj,
    })
```

기본이다! [Paginator.get_page](https://docs.djangoproject.com/en/stable/ref/paginator/#django.core.paginator.Paginator.get_page) 메서드는 template에 필요한 모든 정보가 있는 [Page](https://docs.djangoproject.com/en/stable/ref/paginator/#page-class) 객체를 반환한다. 자세한 내용은 연결된 문서를 참조하시오.

실제 view는 필터링과 순서 지정같은 추가적인 필요가 있을 수 있다. 이러한 작업은 쿼리 문자열 매개 변수에 응답하고 위의 `products` QuerySet을 수정하여 처리할 수 있다.

여기에 페이지내이션을 하기 위한 약간의 표준안이 있다. 페이징을 위한 쿼리 문자열 매개 변수로 `page`를 사용하는 표준화된 관례가 있다면, 이 표준안의 일부를 `paged_object_list_context`(연습으로 남김)와 같은 유틸리티를 사용하여 조금 더 짧게 작성할 수 있다.

```
def product_list(request):
    products = Product.objects.all()
    context = {
       # ...
    } | paged_object_list_context(request, products, paginate_by=5)
    return TemplateResponse(request, 'shop/product_list.html', context)
```

다음: [시작 커스텀 논리 — 위임](customlogicstart.md)

## <a name='discovering-reusable-units-of-code'>토론: 재사용 가능한 코드 단위 찾기</a>
Not yet, Comming Soon!
