Form을 처리하는 view의 기본 패턴은 Django form 문서에 완전히 포함되어 있으므로 몇 가지 참고 사항 외에는 추가할 내용이 없다.

- `FormView`를 사용할 필요가 없으며, 사용하지 않는 것이 좋다.
- 실제로 `Form`도 필요없다. 매우 유용한 일련의 동작(검증 등)을 제공하는 API이지만, 이것 없이도 Django에서 form을 구축하는 것이 전적으로 가능하다. form이 [HTML 수준](https://developer.mozilla.org/en-US/docs/Learn/Forms)에서 어떻게 작동하는지 알고 `request.GET` 또는 `request.POST` 처리해야 하고, 제출된 데이터를 가져오고 이를 통해 무언가를 수행한다.

    일반적으로 `Form`을 사용하는 것에 비해 이 작업은 매우 지루하지만, 경우에 따라 더 나을 수 있다. 예를 들어, 동적으로 생성된 컨트롤(예: 많은 단추 또는 입력 상자)를 사용하는 페이지가 있는 경우 `Form`을 사용하지 않고 컨트롤을 빌드하고 처리하는 것이 가장 쉬울 수 있다.

- 동일한 양식에 다른 작업을 수행하는 여러 버튼이 필요한 경우 HTML 수준에서 어떻게 작동하는지 이해해야 한다. 버튼을 누르면 "성공적인" 컨트롤이 된다. 이는 `request.POST` (또는 `request.GET`) 딕셔너리는 해당 컨트롤의 `name` 어트리뷰트을 가진 항목으로 가져오는 것을 의미한다.

그래서 다음과 같이 보인다:

Template:

```
<form action="" method="POST">
{% csrf_token %}
{{ form }}
<input type="submit" name="save" value="Save">
<input type="submit" name="preview" value="Preview">
</form>
```

View:

```
def my_view(request):
    if request.method == 'POST':
        if 'preview' in request.POST:
            # Do preview thing...
```

한 페이지의 여러 양식에 대해 유사한 작업을 수행해야 할 수도 있다.

이상, 다음은 [Preconditions](preconditions.md)

## <a name='complex-form-cases'>토론: 복잡한 form cases</a>
Not yet, Comming Soon!